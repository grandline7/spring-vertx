# README #

= Vert.x Spring-vertx example project

This project is a demo of exposing Spring-hosted service beans (and therefore Spring-wired services) via vert.x
verticles. In vert.x 3 it's perfectly possible to share an application context between multiple verticles.
In this case the service bean we expose through vert.x is a trivial data access service running through Spring Data
JPA (and using Hibernate). In effect we're taking advantage of the ease of building data access via Spring/Hibernate
but using vert.x as the connector between external clients and the Spring context.
Note that because we're doing blocking JDBC access, we host the call to the service within an executeBlocking call, to
avoid blocking the event bus.
You can run it directly in your IDE by creating a run configuration that uses the main class
`io.vertx.examples.spring.SpringExampleRunner`.
The pom.xml uses the Maven shade plugin to assemble the application and all it's dependencies into a single "fat" jar.
To build a "fat jar"
    mvn package
To run the fat jar:
    java -jar target/spring-vertx-3.3.3-fat.jar
(You can take that jar and run it anywhere there is a Java 8+ JDK. It contains all the dependencies it needs so you
don't need to install Vert.x on the target machine).
Now point your browser at http://localhost:8080 to see a simple welcome page, and then
http://localhost:8080/products to see the Spring-hosted service invoked via the vertx eventbus
=======================================================================================================================

1. 자바 Vert.X HTTP 서버 example 소스
  - java8, spring4.0, STS3.8.1.RELEASE, maven3

2. 원본소스 github
  - https://github.com/vert-x3/vertx-examples/tree/master/spring-examples/spring-example

3. 참고 사이트
  - 기본 개념 : http://d2.naver.com/helloworld/163784
  - 소개 : http://bcho.tistory.com/860
  - 예제 : http://zepinos.blogspot.kr/2016/05/vertx-300-1.html

4. 원본소스 수정항목
  - console log => logback log
  - JPA/hibernate => MyBatis
  - h2 database => postgresql database

5. 소스 실행
  - Vert.x 설치 불필요
  - STS 에서 SpringExampleRunner 파일 실행
  - command 실행
    mvn package
    java -jar target/spring-vertx-3.3.3-fat.jar

6. 테스트
  - 요청 URL : http://localhost:8080, http://localhost:8080/products
  - 응답 데이터 : JSON
  - test tool : SoaUI, JMeter, 크롬 플러그인 RESTful Stress 이용
  - 모니터링 : jconsole, jvisualvm 이용