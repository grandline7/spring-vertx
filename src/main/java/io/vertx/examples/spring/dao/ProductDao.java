package io.vertx.examples.spring.dao;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import io.vertx.examples.spring.domain.Product;

@Repository
public class ProductDao {
	
	@Autowired
	private SqlSession sqlSession;
	
	public List<Product> getAllProducts() {
		List<Product> products = sqlSession.selectList("PRODUCT_MAPPER.getAllProducts");
		return products;
	}
}
