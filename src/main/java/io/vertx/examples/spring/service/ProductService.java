package io.vertx.examples.spring.service;

import io.vertx.examples.spring.dao.ProductDao;
import io.vertx.examples.spring.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Simple Spring service bean to expose the results of a trivial database call
 */
@Service
public class ProductService {

    @Autowired
	private ProductDao productDao;
	
	public List<Product> getAllProducts() {
		return productDao.getAllProducts();
	}

}
