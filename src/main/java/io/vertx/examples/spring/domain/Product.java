package io.vertx.examples.spring.domain;


public class Product {

  private Integer productId;

  private String description;

  public Integer getProductId() {
	  return productId;
  }

  public void setProductId(Integer productId) {
	  this.productId = productId;
  }

  public String getDescription() {
      return description;
  }

  public void setDescription(String description) {
      this.description = description;
  }

}
