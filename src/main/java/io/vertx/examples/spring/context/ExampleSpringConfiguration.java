package io.vertx.examples.spring.context;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;

/**
 * Simple Java Spring configuration to be used for the Spring example application.
 *
 * The Spring service bean and repository are scanned for via @EnableJpaRepositories and @ComponentScan annotations
 */
@Configuration
@PropertySource(value = { "classpath:application.properties" })
@ComponentScan("io.vertx.examples.spring.*")
public class ExampleSpringConfiguration {

  @Autowired
  private Environment env;

  @Bean
  @Autowired
  public DataSource dataSource() {
    final DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
    dataSource.setUrl(env.getProperty("jdbc.url"));
    dataSource.setUsername(env.getProperty("jdbc.username"));
    dataSource.setPassword(env.getProperty("jdbc.password"));
    return dataSource;
  }

  @Bean
  @Autowired
  public SqlSessionFactory sqlSessionFactory() throws Exception {
      SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
      sqlSessionFactoryBean.setDataSource(this.dataSource());
      sqlSessionFactoryBean.setConfigLocation(new ClassPathResource("mybatis-config.xml"));
      return sqlSessionFactoryBean.getObject();
  }

  @Bean
  @Autowired
  public SqlSession sqlSession() throws Exception {
      SqlSession sqlSession = new SqlSessionTemplate(this.sqlSessionFactory());
      return sqlSession;
  }

}
